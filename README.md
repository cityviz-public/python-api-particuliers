# python-api-particuliers
This is an un-official python package that can fetch data such as CAF and IMPÔTS by driving [API Particulier](https://api.gouv.fr/api/api-particulier.html).

## Getting Started
Introduction of installation, basic functions and example of the usage will be presented in this chapter.

### Installation
Package can be installed and upgraded by following commands:
```
# Installation
pip install -i https://test.pypi.org/simple/ api-particuliers
# Upgrade
pip install -i https://test.pypi.org/simple/ api-particuliers --upgrade
```
Considering of security issues, setting token/API key as environment vairables is highly recommended. 
```python
import os
# Declare and initial your KEY with environment variables.
API_KEY = os.environ['API_KEY']
```

### Introduction of package
Two classes, Caf and Impots, are created respectively. Both of them require api_key to initial themselves.
```python
Caf(api_key)/Impots(api_key)
```
Functions:
* **get_response** (numero_allocataire/numero_Fiscal, code_postal/reference_Avis, x_user or None). It will return a requests object with status_code, content, etc...
* **get_caf** and **get_impots** (numero_allocataire/numero_Fiscal, code_postal/reference_Avis, x_user or None). It will return the whole data associated with certain "numero allocataire" or "numero fiscal" in JSON.
* **get_data** (data_type, numero_allocataire/numero_Fiscal, code_postal/reference_Avis, x_user or None). It will return one or a list of selected data.

Your data_type may be like this:
```
select_one_data = "allocataires"

a_list_data = ["allocataires", "enfants", "adresse", "quotientFamilial", "annee", "mois"]

```

Returned data will be an expected data or error, thus we recommed your to test whether it is your expected result or not before declare a new variable with your result.
```python
# example
try:
    print(response['error'])
except KeyError:
    print(response)
```



### Usage

This is an example for getting data from CAF.
```python
from particulier_api import Caf

# Token and parameters are used for test only
numeroAllocataire = "0000354"
codePostal = "99148"
# We recommend to set key like this:
# CAF_KEY = os.environ['CAF_KEY']
caf_key = "3841b13fa8032ed3c31d160d3437a76a"
# You may get a response with following data types when you call a caf
data_wanted = ["allocataires", "enfants"]
# Create your caf
your_caf = Caf(caf_key)
# Select data expected with function get_data or complete information with get_caf
response_list = your_caf.get_data(data_wanted, numeroAllocataire, codePostal)
print(response_list)

```

## Notice
Please visit [cityviz](https://www.cityviz.io) and ask for help if you have any problems about this package.
And for more details about API Particulier, please visite [api particulier website](https://api.gouv.fr/api/api-particulier.html) 
