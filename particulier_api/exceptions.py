import requests


class ParticulierAPIError:

    def __init__(self, response):
        self._response = response

    def describe_error(self, service_type=None, data_type=None):
        if not _is_server_connected(service_type):
            reason = "Server disconnected."
            if service_type:
                message = "Service" + service_type + "is not available now, try again latter."
            else:
                message = "API is not available now, try again latter."
            error = "Service error"
            return {"error": error,
                    "reason": reason,
                    "message": message}

        if not _is_response_successful(self._response.status_code):
            try:
                return self._response.json()
            except:
                reason = "API key is not correct."
                if service_type:
                    message = "Le token ne vous permet pas d’accéder à " + service_type
                else:
                    message = "Le token ne vous permet pas d’accéder à l’API"
                error = "Le token ne vous permet pas d’accéder à l’API"

                return {"error": error,
                        "reason": reason,
                        "message": message}

        if data_type:
            try:
                self._response.json()[data_type]
            except:
                reason = "Data is not available."
                message = "This data doesn't exist, please check."
                error = "No such data in response"
                return {"error": error,
                        "reason": reason,
                        "message": message}

        return None


def parameter_type_error(parameters):

    for parameter in parameters:
        if type(parameter) is list:
            for sub_parameter in parameter:
                if type(sub_parameter) is not str:
                    raise TypeError("Parameters must be string, %s is detected." % type(sub_parameter))
        elif type(parameter) is not str:
            raise TypeError("Parameters must be string or list, %s is detected." % type(parameter))


def _is_server_connected(service):
    headers = {'accept': 'application/json'}
    if service:
        path_required = "https://particulier-test.api.gouv.fr/api/{}/ping".format(service)
    else:
        path_required = "https://particulier-test.api.gouv.fr/api/ping"
        # Si le résultat est autre qu’un code HTTP 200, le serveur de la CAF rencontre un problème.
    status = requests.get(path_required, headers=headers).status_code
    if _is_response_successful(status):
        # On peut alors considérer que les résultat du endpoint /caf/famille seront perturbés.
        return True

    return False


def _is_response_successful(status):
    if status != 200:
        return False
    return True
