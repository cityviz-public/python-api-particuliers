import requests as r

from .exceptions import *


class Caf:

    def __init__(self, api_key):
        self._api_key = api_key
        self._url_response = None
        self._caf = None
        self._numero_allocataire = "_None"
        self._code_postal = "_None"

    def get_response(self, numero_allocataire, code_postal, x_user=None):
        # parameters must be string or list
        parameter_type_error([numero_allocataire, code_postal])

        if x_user:
            headers = {'accept': 'application/json', 'X-API-Key': self._api_key, 'X-User': x_user}
        else:
            headers = {'accept': 'application/json', 'X-API-Key': self._api_key}

        if self._numero_allocataire != numero_allocataire:
            # parameters are changed, self.parameters need to be re-defined
            self._numero_allocataire = numero_allocataire
            self._code_postal = code_postal
        else:
            try:
                # if last response is an error, we need to fetch it again from API
                error_message = self._caf['error']
                pass
            except KeyError:
                # we dont need to ask API, required response is same as self._caf
                # bug: it return a json type data.
                # fix: under this condition, we should return a request type.
                return self._url_response

        path_required = "/caf/famille?numeroAllocataire={}&codePostal={}".format(self._numero_allocataire,
                                                                                 self._code_postal)
        url = _url(path_required)
        api_response = r.get(url, headers=headers)
        error_message = ParticulierAPIError(api_response).describe_error("caf")
        if error_message:
            self._caf = error_message
        else:
            self._caf = api_response.json()

        self._url_response = api_response
        return api_response

    def get_caf(self, numero_allocataire, code_postal, x_user=None):
        self.get_response(numero_allocataire, code_postal, x_user)
        return self._caf

    def get_data(self, data_type, numero_allocataire, code_postal, x_user=None):
        # parameters must be string or list
        parameter_type_error(data_type)
        response = self.get_response(numero_allocataire, code_postal, x_user)

        # check api key is available or not
        error_message = ParticulierAPIError(response).describe_error("caf")
        if error_message:
            return error_message

        data_required = {}
        if type(data_type) is list:
            # if require a list of data
            for required_type in data_type:
                try:
                    data_required[required_type] = self._caf[required_type]
                except KeyError:
                    data_required[required_type] = "This data is not available."
        else:
            # if require one data only
            error_message = ParticulierAPIError(response).describe_error("caf", data_type)
            if error_message:
                return error_message
            else:
                data_required[data_type] = self._caf[data_type]

        return data_required


class Impots:

    def __init__(self, api_key):
        self._api_key = api_key
        self._url_response = None
        self._impots = None
        self._numero_fiscal = "_None"
        self._reference_avis = "_None"

    def get_response(self, numero_fiscal, reference_avis, x_user=None):
        # parameters must be string or list
        parameter_type_error([numero_fiscal, reference_avis])

        if x_user:
            headers = {'accept': 'application/json', 'X-API-Key': self._api_key, 'X-User': x_user}
        else:
            headers = {'accept': 'application/json', 'X-API-Key': self._api_key}

        if self._numero_fiscal != numero_fiscal:
            # parameters are changed, self.parameters need to be refreshed
            self._numero_fiscal = numero_fiscal
            self._reference_avis = reference_avis
        else:
            try:
                # if last response is an error, we need to demand it again
                error_message = self._impots['error']
                pass
            except KeyError:
                # we dont need to ask API, self._impots is always the same.
                # bug: it return a json type data.
                # fix: under this condition, we should return a request type.
                return self._url_response

        path_required = "/impots/svair?numeroFiscal={}&referenceAvis={}".format(self._numero_fiscal,
                                                                                self._reference_avis)
        url = _url(path_required)
        api_response = r.get(url, headers=headers)
        error_message = ParticulierAPIError(api_response).describe_error("impots")
        if error_message:
            self._impots = error_message
        else:
            self._impots = api_response.json()

        self._url_response = api_response
        return api_response

    def get_impots(self, numero_fiscal, reference_avis, x_user=None):
        self.get_response(numero_fiscal, reference_avis, x_user)
        return self._impots

    def get_data(self, data_type, numero_fiscal, reference_avis, x_user=None):
        # parameters must be string or list
        parameter_type_error(data_type)
        response = self.get_response(numero_fiscal, reference_avis, x_user)

        # check api key is available or not
        error_message = ParticulierAPIError(response).describe_error("impots")
        if error_message:
            return error_message

        data_required = {}
        if type(data_type) is list:
            # if require a list of data
            for required_type in data_type:
                try:
                    data_required[required_type] = self._impots[required_type]
                except KeyError:
                    data_required[required_type] = "This data is not available."
        else:
            # if require one data only
            error_message = ParticulierAPIError(response).describe_error("impots", data_type)
            if error_message:
                return error_message
            else:
                data_required[data_type] = self._impots[data_type]

        return data_required


def _url(path):
    return 'https://particulier-test.api.gouv.fr/api' + path
