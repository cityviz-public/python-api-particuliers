from setuptools import setup, find_packages


with open("README.md", "r") as readme_file:
    readme = readme_file.read()

requirements = ["requests>=2"]

setup(
    name="api-particuliers",
    version="0.0.5",
    author="Fan Yang",
    author_email="fan@cityiviz.io",
    description="A package for API Particulier",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/cityviz-public/python-api-particuliers",
    packages=find_packages(),
    install_requires=requirements,
    classifiers=[
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
)