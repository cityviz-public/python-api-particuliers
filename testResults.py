from particulier_api import *


def runtest():
    numeroAllocataire = "0000354"
    codePostal = "99148"
    test_caf_key = "3841b13fa8032ed3c31d160d3437a76a"
    test_caf = Caf(test_caf_key)
    test_123 = test_caf.get_caf(numeroAllocataire, codePostal)
    response_caf = test_caf.get_data("quotientFamilial", numeroAllocataire, codePostal)
    print(test_123)

    numeroFiscal = "1702599999001"
    referenceAvis = "1702599999001"
    test_impots_key = "83c68bf0b6013c4daf3f8213f7212aa5"
    test_data_wanted = ["1223", "declarant2", "declarant1"]
    test_impots = Impots(test_impots_key)
    test_321 = test_impots.get_impots(numeroFiscal, referenceAvis)
    response_impots = test_impots.get_data(test_data_wanted, numeroFiscal,referenceAvis)
    print(test_321)

runtest()