from particulier_api import *


class TestCaf:

    numeroAllocataire = "0000354"
    codePostal = "99148"
    test_caf_key = "3841b13fa8032ed3c31d160d3437a76a"
    caf_data_types = ["allocataires", "enfants", "adresse", "quotientFamilial", "annee", "mois"]

    def test_caf_get_response(self):

        test_caf = Caf(self.test_caf_key)
        response = test_caf.get_response(self.numeroAllocataire, self.codePostal)
        message = ParticulierAPIError(response).describe_error()
        assert response.status_code == 200, message['message']

    def test_get_caf(self):
        test_caf = Caf(self.test_caf_key)
        response = test_caf.get_caf(self.numeroAllocataire, self.codePostal)
        try:
            assert response['message'] == '', response['message']
        except (TypeError, KeyError):
            pass

    def test_caf_get_data(self):
        test_caf = Caf(self.test_caf_key)
        response = test_caf.get_data(self.caf_data_types, self.numeroAllocataire, self.codePostal)
        try:
            assert response['message'] == '', response['message']
        except (TypeError, KeyError):
            pass



class TestImpots:

    numeroFiscal = "1702599999001"
    referenceAvis = "1702599999001"
    test_impots_key = "83c68bf0b6013c4daf3f8213f7212aa5"
    impots_data_types = ["declarant1", "declarant2", "dateRecouvrement", "dateEtablissement", "nombreParts", "situationFamille",
                      "nombrePersonnesCharge", "revenuBrutGlobal", "revenuImposable", "impotRevenuNetAvantCorrections",
                      "montantImpot", "revenuFiscalReference", "foyerFiscal", "anneeImpots", "anneeRevenus", "situationPartielle"]

    def test_impots_get_response(self):

        test_impots = Impots(self.test_impots_key)
        response = test_impots.get_response(self.numeroFiscal, self.referenceAvis)
        message = ParticulierAPIError(response).describe_error()
        assert response.status_code == 200, message['message']

    def test_get_caf(self):
        test_impots = Impots(self.test_impots_key)
        response = test_impots.get_impots(self.numeroFiscal, self.referenceAvis)
        try:
            assert response['message'] == '', response['message']
        except (TypeError, KeyError):
            pass

    def test_impots_get_data(self):
        test_impots = Impots(self.test_impots_key)
        response = test_impots.get_data(self.impots_data_types, self.numeroFiscal, self.referenceAvis)
        try:
            assert response['message'] == '', response['message']
        except (TypeError, KeyError):
            pass


